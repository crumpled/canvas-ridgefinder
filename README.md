# Find Ridgeline from Digital Elevation Map

## Instructions
Open index.html, or run `npm start`
Click on the hill shade image, 360 degree ridgeline (from the perspective of the clicked location) is rendered, client-side in an SVG.

Be patient. It can take several seconds, especially with a high `RESOLUTION` or `DISTANCE` value.

## Variables
you can edit some of the CONSTANTS in the code to get different results.
* `RESOLUTION`: How many compass points should we examine? `360` would mean we would take samples every 1 degree.
* `VISIBILITY`: How far should we examine? `200` would mean we look 200 pixels into the distance, and the ridgeline SVG will have 200 compound lines.
* `HIGHEST`: What is the real life elevation (in meters) for a white pixel in our DEM?
* `LOWEST`: What is the real life elevation (in meters) for a black pixel in our DEM?

Note: `HIGHEST` and `LOWEST` are only used to return the approximate real life elevation of the clicked location in the console.
